package zzpubs

import (
	"fmt"
	"mime/multipart"
	"net/http"

	"bitbucket.org/colorsocean/oppagovnosite/3dgp/3dgp/utils"
	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
	"bitbucket.org/colorsocean/s2"
	"github.com/zenazn/goji/web"
)

type ReqList struct {
	Page int
}

type RespList struct {
	List  []*Publication
	Page  int
	Total int
}

func ListHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqList
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}
		if req.Page < 1 {
			req.Page = 1
		}

		var resp RespList

		const pageSize = 10

		resp.Page = req.Page
		resp.List, resp.Total, err = svc.List((req.Page-1)*pageSize, pageSize)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqGet struct {
	ID int64
}

type RespGet struct {
	Item *Publication
}

func GetHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqGet
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespGet

		resp.Item, err = svc.Get(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqDelete struct {
	ID int64
}

type RespDelete struct {
}

func DeleteHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqDelete
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespDelete

		err = svc.Delete(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqPut struct {
	ID       int64
	Item     *Publication
	NewImage *multipart.FileHeader

	NewResources []*multipart.FileHeader
}

type RespPut struct {
	Item *Publication
}

func PutHandler(svc *Service, stor *s2.Storage) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqPut
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespPut

		var newImageFull *utils.FileInfo
		var newImageThumb *utils.FileInfo
		var newResources []*utils.FileInfo

		if req.NewImage != nil {
			orig, err := req.NewImage.Open()
			if err != nil {
				panic(err)
			}
			defer orig.Close()
			storeOp := s2.Store{Name: req.NewImage.Filename, MIME: req.NewImage.Header.Get("Content-Type")}
			storeOp.R = orig
			fileOrig, err := stor.Store(storeOp)
			if err != nil {
				panic(err)
			}
			_, err = orig.Seek(0, 0)
			if err != nil {
				panic(err)
			}

			thumb, err := utils.MkThumb(orig, 907, 484)
			if err != nil {
				panic(err)
			}
			storeOp.R = thumb
			fileThumb, err := stor.Store(storeOp)
			if err != nil {
				panic(err)
			}

			newImageFull = utils.NewFileInfo(fileOrig)
			newImageThumb = utils.NewFileInfo(fileThumb)
		}

		for _, res := range req.NewResources {
			file, err := stor.StoreMultipart(s2.StoreMultipart{File: res})
			if err != nil {
				panic(err)
			}
			newResources = append(newResources, utils.NewFileInfo(file))
		}
		fmt.Println("HANDLER OKAY 4")

		resp.Item, err = svc.Put(req.ID, req.Item, newImageFull, newImageThumb, newResources)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}
