package zzpubs

import (
	"time"

	"bitbucket.org/colorsocean/oppagovnosite/3dgp/3dgp/utils"
	"bitbucket.org/colorsocean/puzzles/cid"
	"gopkg.in/mgo.v2"
)

const pageSize = 10

type Publication struct {
	ID         int64           `bson:"_id"`
	ImageFull  *utils.FileInfo `bson:",omitempty"`
	ImageThumb *utils.FileInfo `bson:",omitempty"`
	Title      string
	Content    string
	Created    time.Time

	Resources []*utils.FileInfo `bson:",omitempty"`
}

type Service struct {
	coll cid.CollFn
	cid  *cid.Service
}

func NewService(coll cid.CollFn, cid *cid.Service) (svc *Service, err error) {
	svc = &Service{
		coll: coll,
		cid:  cid,
	}

	return
}

func (this *Service) cpubs(actor func(coll *mgo.Collection) error) error {
	return this.coll("publications", actor)
}

func (this *Service) List(skip, take int) (list []*Publication, total int, err error) {
	err = this.cpubs(func(coll *mgo.Collection) error {
		n, err := coll.Find(nil).Count()
		if err != nil {
			panic(err)
			return err
		}
		total = n
		err = coll.Find(nil).Sort("-created").Skip(skip).Limit(take).All(&list)
		if err != nil {
			panic(err)
			return err
		}
		return nil
	})
	return
}

func (this *Service) Get(id int64) (news *Publication, err error) {
	err = this.cpubs(func(coll *mgo.Collection) error {
		return coll.FindId(id).One(&news)
	})
	return
}

func (this *Service) Delete(id int64) (err error) {
	err = this.cpubs(func(coll *mgo.Collection) error {
		return coll.RemoveId(id)
	})
	return
}

func (this *Service) Put(pubID int64, pub *Publication, newImageFull, newImageThumb *utils.FileInfo, newRes []*utils.FileInfo) (*Publication, error) {
	return pub, this.cpubs(func(coll *mgo.Collection) (err error) {
		if pubID == 0 {
			id, err := this.cid.Obtain("publications")
			if err != nil {
				return err
			}
			pubID = id
		}
		if pub != nil {
			if pub.ID == 0 {
				pub.Created = time.Now().UTC()
			}
			pub.ID = pubID
		} else {
			pub, err = this.Get(pubID)
			if err != nil {
				pub = &Publication{
					ID:      pubID,
					Created: time.Now().UTC(),
				}
			}
		}

		if newImageFull != nil {
			pub.ImageFull = newImageFull
		}
		if newImageThumb != nil {
			pub.ImageThumb = newImageThumb
		}

		if len(newRes) > 0 {
			for _, res := range newRes {
				pub.Resources = append(pub.Resources, res)
			}
		}

		_, err = coll.UpsertId(pub.ID, pub)
		if err != nil {
			return err
		}

		pub, err = this.Get(pubID)
		if err != nil {
			return err
		}
		return err
	})
}
