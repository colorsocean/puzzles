package zzempls

import (
	"fmt"
	"strings"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/colorsocean/oppagovnosite/3dgp/3dgp/utils"
	"bitbucket.org/colorsocean/puzzles/cid"
)

type Service struct {
	coll cid.CollFn
	cid  *cid.Service
}

func NewService(coll cid.CollFn, cid *cid.Service) (svc *Service, err error) {
	svc = &Service{
		coll: coll,
		cid:  cid,
	}

	err = svc.cempls(func(coll *mgo.Collection) error {

		return coll.EnsureIndex(mgo.Index{
			Key: []string{
				"$text:fullname",
				"$text:qualification",
				"$text:jobposition",
			},
			DefaultLanguage: "ru",
		})
	})

	return
}

func (this *Service) cempls(actor func(coll *mgo.Collection) error) error {
	return this.coll("employees", actor)
}

func (this *Service) Put(emplID int64, empl *Employee, newPhotoFull, newPhotoThumb *utils.FileInfo, newRes []*utils.FileInfo) (item *Employee, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		if emplID == 0 {
			id, err := this.cid.Obtain("employees")
			if err != nil {
				panic(err)
				return err
			}
			emplID = id
		}

		if empl != nil {
			empl.ID = emplID
		} else {
			empl, err = this.Get(emplID)
			if err != nil {
				empl = &Employee{
					ID: emplID,
				}
			}
		}

		item = empl.Compute()
		if item.Type.String() == "" {
			item.Type = Adm
		}
		//> This fields will be updated via separate methods
		//item.Resources = nil
		if newPhotoThumb != nil {
			item.PhotoThumb = newPhotoThumb
		}
		if newPhotoFull != nil {
			item.PhotoFull = newPhotoFull
		}

		if len(newRes) > 0 {
			for _, res := range newRes {
				item.Resources = append(item.Resources, res)
			}
		}

		_, err = coll.UpsertId(item.ID, item)
		if err != nil {
			panic(err)
			return err
		}

		empl, err = this.Get(item.ID)
		if err != nil {
			panic(err)
			return err
		}

		return nil
	})
	return
}

func (this *Service) Delete(id int64) (err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		return coll.RemoveId(id)
	})
	return
}

func (this *Service) Get(id int64) (employee *Employee, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		var empl Employee
		err := coll.FindId(id).One(&empl)
		if err != nil {
			return err
		}
		employee = empl.Compute()
		return nil
	})
	return
}

func (this *Service) Query(query string, skip, take int) (list []*Employee, total int, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		q := coll.FindId(bson.M{
			"$text": bson.M{
				"$search": query,
			},
		}).Skip(skip).Limit(take)

		t, err := q.Count()
		if err != nil {
			return err
		}
		total = t

		return q.All(&list)
	})
	return
}

func (this *Service) list(q bson.M, sort string, skip, take int) (list []*Employee, total int, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		n, err := coll.Find(q).Count()
		if err != nil {
			return err
		}
		total = n
		return coll.Find(q).Sort("lastname").Skip(skip).Limit(take).All(&list)
	})
	if err == nil {
		for i := 0; i < len(list); i++ {
			list[i] = list[i].Compute()
		}
	}
	return
}

func (this *Service) List(et EmployeeType, haveReception bool, skip, take int) (list []*Employee, total int, err error) {
	var q bson.M = nil
	if et.String() != "" {
		if q == nil {
			q = bson.M{}
		}
		q["type"] = et.String()
	}
	if haveReception {
		if q == nil {
			q = bson.M{}
		}
		q["havereception"] = true
	}
	return this.list(q, "lastname", skip, take)
}

func (this *Service) ListByFirstNameLetter(letter string, skip, take int) (list []*Employee, total int, err error) {
	return this.list(bson.M{
		"firstname": letter,
	}, "firstname", skip, take)
}

func (this *Service) ListByLastNameLetter(letter string, skip, take int) (list []*Employee, total int, err error) {
	return this.list(bson.M{
		"lastname": letter,
	}, "lastname", skip, take)
}

func (this *Service) AddResource(emplID int64, fi *utils.FileInfo) (employee Employee, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		err := coll.UpdateId(emplID, bson.M{
			"$push": bson.M{
				"resources": fi,
			},
		})
		if err != nil {
			return err
		}

		err = coll.FindId(emplID).One(&employee)
		if err != nil {
			return err
		}

		return nil
	})
	return
}

func (this *Service) RemoveResource(emplID int64, resID string) (employee *Employee, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		err := coll.UpdateId(emplID, bson.M{
			"$pull": bson.M{
				"resources": bson.M{
					"storageid": resID,
				},
			},
		})
		if err != nil {
			return err
		}

		err = coll.FindId(emplID).One(&employee)
		if err != nil {
			return err
		}

		return nil
	})
	return
}

func (this *Service) SetPhoto(emplID int64, thumb, full string) (employee Employee, err error) {
	err = this.cempls(func(coll *mgo.Collection) error {
		err = coll.UpdateId(emplID, bson.M{
			"$set": bson.M{
				"photothumbid": thumb,
				"photofullid":  full,
			},
		})
		if err != nil {
			return err
		}

		err = coll.FindId(emplID).One(&employee)
		if err != nil {
			return err
		}

		return nil
	})
	return
}

type EmployeeType string

func (this EmployeeType) String() string {
	return string(this)
}

const (
	Adm EmployeeType = "adm"
	Mid EmployeeType = "mid"
	Doc EmployeeType = "doc"
)

type Employee struct {
	ID int64 `bson:"_id"`

	FirstName     string
	MiddleName    string
	LastName      string
	Gender        string
	Qualification string
	JobPosition   string
	Information   string

	PhotoThumb *utils.FileInfo `bson:",omitempty"`
	PhotoFull  *utils.FileInfo `bson:",omitempty"`

	Resources []*utils.FileInfo `bson:",omitempty"`

	Type EmployeeType

	HaveReception bool
	WorkHours     []EmployeeWorkHours

	FullName    string
	FirstName1  string `json:"-"`
	MiddleName1 string `json:"-"`
	LastName1   string `json:"-"`
}

type EmployeeWorkHours struct {
	StartHours   string
	StartMinutes string
	EndHours     string
	EndMinutes   string
	Workplace    string
}

func (this *Employee) Compute() *Employee {
	this.FullName = fmt.Sprintf("%s %s %s", this.LastName, this.FirstName, this.MiddleName)
	this.FullName = strings.TrimSpace(this.FullName)
	this.FullName = strings.Replace(this.FullName, "  ", " ", -1)

	if len(this.FirstName) > 0 {
		this.FirstName1 = this.FirstName[0:2]
	}
	if len(this.MiddleName) > 0 {
		this.MiddleName1 = this.MiddleName[0:2]
	}
	if len(this.LastName) > 0 {
		this.LastName1 = this.LastName[0:2]
	}

	if len(this.WorkHours) != 6 {
		this.WorkHours = []EmployeeWorkHours{
			{"08", "00", "17", "00", "0"},
			{"08", "00", "17", "00", "0"},
			{"08", "00", "17", "00", "0"},
			{"08", "00", "17", "00", "0"},
			{"08", "00", "17", "00", "0"},
			{"08", "00", "17", "00", "0"},
		}
	}

	return this
}
