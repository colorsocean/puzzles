package zzempls

import (
	"fmt"
	"mime/multipart"
	"net/http"

	"bitbucket.org/colorsocean/oppagovnosite/3dgp/3dgp/utils"
	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
	"bitbucket.org/colorsocean/s2"
	"github.com/zenazn/goji/web"
)

const pageSize = 20

func ListRequestHelper(svc *Service, req ReqList, haveReception bool) (list []*Employee, page, total int, err error) {
	if req.Page < 1 {
		req.Page = 1
	}
	page = req.Page

	if req.Letter != "" {
		list, total, err = svc.ListByLastNameLetter(req.Letter, (req.Page-1)*pageSize, pageSize)
	} else if req.Query != "" {
		list, total, err = svc.Query(req.Query, (req.Page-1)*pageSize, pageSize)
	} else {
		list, total, err = svc.List(req.Type, haveReception, (req.Page-1)*pageSize, pageSize)
	}
	return
}

type ReqList struct {
	Type   EmployeeType
	Page   int
	Letter string
	Query  string
}

type RespList struct {
	List  []*Employee
	Page  int
	Total int
}

func ListHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqList
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespList

		req.Type = EmployeeType("")

		resp.List, resp.Page, resp.Total, err = ListRequestHelper(svc, req, false)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqPut struct {
	ID   int64
	Item *Employee

	NewPhoto *multipart.FileHeader

	NewResources []*multipart.FileHeader
}

type RespPut struct {
	Item *Employee
}

func PutHandler(svc *Service, stor *s2.Storage) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqPut
		fmt.Println("HANDLER OKAY 1")
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespPut

		var (
			newPhotoFull  *utils.FileInfo
			newPhotoThumb *utils.FileInfo
			newResources  []*utils.FileInfo
		)

		fmt.Println("HANDLER OKAY 2")
		if req.NewPhoto != nil {
			orig, err := req.NewPhoto.Open()
			if err != nil {
				panic(err)
			}
			defer orig.Close()
			storeOp := s2.Store{Name: req.NewPhoto.Filename, MIME: req.NewPhoto.Header.Get("Content-Type")}
			storeOp.R = orig
			fileOrig, err := stor.Store(storeOp)
			if err != nil {
				panic(err)
			}
			_, err = orig.Seek(0, 0)
			if err != nil {
				panic(err)
			}

			thumb, err := utils.MkThumb(orig, 427, 568)
			if err != nil {
				panic(err)
			}
			storeOp.R = thumb
			fileThumb, err := stor.Store(storeOp)
			if err != nil {
				panic(err)
			}

			newPhotoFull = utils.NewFileInfo(fileOrig)
			newPhotoThumb = utils.NewFileInfo(fileThumb)
		}
		fmt.Println("HANDLER OKAY 3")

		for _, res := range req.NewResources {
			file, err := stor.StoreMultipart(s2.StoreMultipart{File: res})
			if err != nil {
				panic(err)
			}
			newResources = append(newResources, utils.NewFileInfo(file))
		}
		fmt.Println("HANDLER OKAY 4")

		resp.Item, err = svc.Put(req.ID, req.Item, newPhotoFull, newPhotoThumb, newResources)
		if err != nil {
			panic(err)
		}

		fmt.Println("HANDLER OKAY 5")

		response.C(c).Payload(resp)
	}
}

type ReqRemResource struct {
	ID    int64
	ResID string
}

type RespRemResource struct {
	Item *Employee
}

func RemResourceHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqRemResource
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespRemResource

		resp.Item, err = svc.RemoveResource(req.ID, req.ResID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqGet struct {
	ID int64
}

type RespGet struct {
	Item *Employee
}

func GetHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqGet
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespGet

		resp.Item, err = svc.Get(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqDelete struct {
	ID int64
}

type RespDelete struct {
}

func DeleteHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqDelete
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespDelete

		err = svc.Delete(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}
