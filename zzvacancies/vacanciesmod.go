package zzvacancies

import (
	"net/http"

	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
	"github.com/zenazn/goji/web"
)

type ReqList struct {
	Page int
}

type RespList struct {
	List  []*Vacancy
	Page  int
	Total int
}

func ListHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqList
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}
		if req.Page < 1 {
			req.Page = 1
		}

		var resp RespList

		const pageSize = 10

		resp.Page = req.Page
		resp.List, resp.Total, err = svc.List((req.Page-1)*pageSize, pageSize)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqGet struct {
	ID int64
}

type RespGet struct {
	Item *Vacancy
}

func GetHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqGet
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespGet

		resp.Item, err = svc.Get(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqPut struct {
	Item *Vacancy
}

type RespPut struct {
	Item *Vacancy
}

func PutHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqPut
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespPut

		resp.Item, err = svc.Put(req.Item)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqDelete struct {
	ID int64
}

type RespDelete struct {
}

func DeleteHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqDelete
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespDelete

		err = svc.Delete(req.ID)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}
