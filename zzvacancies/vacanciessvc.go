package zzvacancies

import (
	"time"

	"bitbucket.org/colorsocean/puzzles/cid"
	"gopkg.in/mgo.v2"
)

type Vacancy struct {
	ID int64 `bson:"_id"`

	Created time.Time
	Updated time.Time

	Position     string
	Salary       string
	Requirements string
	Conditions   string
}

type Service struct {
	coll cid.CollFn
	cid  *cid.Service
	name string
}

func NewService(coll cid.CollFn, cid *cid.Service) (svc *Service, err error) {
	svc = &Service{
		coll: coll,
		cid:  cid,
		name: "vacancies",
	}

	return
}

func (this *Service) cvacancies(actor func(coll *mgo.Collection) error) error {
	return this.coll(this.name, actor)
}

func (this *Service) List(skip, take int) (list []*Vacancy, total int, err error) {
	err = this.cvacancies(func(coll *mgo.Collection) error {
		n, err := coll.Find(nil).Count()
		if err != nil {
			panic(err)
			return err
		}
		err = coll.Find(nil).Sort("position").Skip(skip).Limit(take).All(&list)
		if err != nil {
			panic(err)
			return err
		}
		total = n
		return nil
	})
	return
}

func (this *Service) Get(id int64) (item *Vacancy, err error) {
	err = this.cvacancies(func(coll *mgo.Collection) error {
		err := coll.FindId(id).One(&item)
		if err != nil {
			panic(err)
			return err
		}
		return nil
	})
	return
}

func (this *Service) Delete(id int64) (err error) {
	err = this.cvacancies(func(coll *mgo.Collection) error {
		err := coll.RemoveId(id)
		if err != nil {
			panic(err)
			return err
		}
		return nil
	})
	return
}

func (this *Service) Put(item *Vacancy) (*Vacancy, error) {
	return item, this.cvacancies(func(coll *mgo.Collection) error {
		if item.ID == 0 {
			id, err := this.cid.Obtain(this.name)
			if err != nil {
				panic(err)
				return err
			}
			item.ID = id
			item.Created = time.Now().UTC()
			item.Updated = item.Created
		} else {
			item.Updated = time.Now().UTC()
		}

		_, err := coll.UpsertId(item.ID, item)
		if err != nil {
			panic(err)
			return err
		}
		return nil
	})
}
