package zzsurvey

import (
	"net/http"

	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
	"github.com/zenazn/goji/web"
)

type ReqList struct {
	TypeID string
	Page   int
}

type RespList struct {
	List   []*Survey
	TypeID string
	Page   int
	Total  int
}

func ListHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqList
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}
		if req.Page < 1 {
			req.Page = 1
		}
		if req.TypeID == "" {
			req.TypeID = "visit"
		}

		var resp RespList

		const pageSize = 10

		resp.Page = req.Page
		resp.TypeID = req.TypeID
		resp.List, resp.Total, err = svc.List(req.TypeID, (req.Page-1)*pageSize, pageSize)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}

type ReqPut struct {
	Item *Survey
}

type RespPut struct {
	Item *Survey
}

func PutHandler(svc *Service) web.HandlerFunc {
	return func(c web.C, w http.ResponseWriter, q *http.Request) {
		var req ReqPut
		_, err := rtool.Read(rtool.Options{Out: &req, W: w, Q: q, Validate: true})
		if err != nil {
			panic(err)
		}

		var resp RespPut

		resp.Item, err = svc.Put(req.Item)
		if err != nil {
			panic(err)
		}

		response.C(c).Payload(resp)
	}
}
