package zzsurvey

import (
	"time"

	"bitbucket.org/colorsocean/puzzles/cid"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Survey struct {
	ID     bson.ObjectId `bson:"_id"`
	TypeID string

	Started   time.Time
	Created   time.Time
	Title     string
	FullName  string
	Phone     string
	Email     string
	Questions []Question
}

type Question struct {
	Text           string
	MultiSelection bool
	Variants       []Variant
}

type Variant struct {
	Selected bool
	Parts    []VariantPart
}

type VariantType string

const (
	VariantLabel  VariantType = "label"
	VariantText   VariantType = "text"
	VariantNumber VariantType = "number"
)

type VariantPart struct {
	Type  VariantType
	Value string
}

type Service struct {
	coll cid.CollFn
}

func NewService(coll cid.CollFn) (svc *Service, err error) {
	svc = &Service{
		coll: coll,
	}

	return
}

func (this *Service) csurvs(actor func(coll *mgo.Collection) error) error {
	return this.coll("surveys", actor)
}

func (this *Service) List(typeID string, skip, take int) (list []*Survey, total int, err error) {
	err = this.csurvs(func(coll *mgo.Collection) error {
		q := bson.M{
			"typeid": typeID,
		}
		n, err := coll.Find(q).Count()
		if err != nil {
			panic(err)
			return err
		}
		err = coll.Find(q).Sort("-created").Skip(skip).Limit(take).All(&list)
		if err != nil {
			panic(err)
			return err
		}
		total = n
		return nil
	})
	return
}

func (this *Service) Put(item *Survey) (*Survey, error) {
	return item, this.csurvs(func(coll *mgo.Collection) error {
		item.ID = bson.NewObjectId()
		item.Created = time.Now().UTC()
		if item.TypeID == "" {
			item.TypeID = "visit"
		}

		err := coll.Insert(item)
		if err != nil {
			panic(err)
			return err
		}
		return nil
	})
}
