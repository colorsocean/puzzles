package zzgal

import (
	"time"

	"bitbucket.org/colorsocean/puzzle"
	"bitbucket.org/colorsocean/puzzles/cid"
	"bitbucket.org/colorsocean/s2"
	"gopkg.in/mgo.v2"
)

type GalleryImage struct {
	Full  *s2.FileInfo
	Thumb *s2.FileInfo
}

type Gallery struct {
	ID         int64
	LongID     string
	CoverFull  *s2.FileInfo
	CoverThumb *s2.FileInfo
	Title      string
	Content    string
	Created    time.Time
}

type Service struct {
	coll puzzle.CollFn
	cid  *cid.Service
}

func NewService() (svc *Service, err error) {
	svc = &Service{
		coll: coll,
		cid:  cid,
	}

	return
}

func (this *Service) cpubs(actor func(coll *mgo.Collection) error) error {
	return this.coll("galleries", actor)
}
