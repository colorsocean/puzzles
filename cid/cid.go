package cid

import (
	"log"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CollFn func(string, func(col *mgo.Collection) error) error

type Service struct {
	coll CollFn
	name string
}

func NewService(coll CollFn) *Service {
	svc := &Service{
		coll: coll,
		name: "cid",
	}

	return svc
}

func (this Service) c(fn func(col *mgo.Collection) error) error {
	return this.coll(this.name, fn)
}

func (this Service) Obtain(name string) (cid int64, err error) {
	err = this.c(func(col *mgo.Collection) (err error) {
		doc := &struct {
			ID  string `bson:"_id,omitempty"`
			CID int64
		}{
			CID: 1,
		}

		id := this.name
		if name != "" {
			id = name
		}

		_, err = col.Find(bson.M{"_id": id}).Apply(mgo.Change{
			Update:    bson.M{"$inc": doc},
			ReturnNew: true,
			Upsert:    true,
		}, doc)

		log.Println("DOC:", doc)

		if err != nil {
			return
		}

		cid = doc.CID

		return
	})

	return
}
